﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace erlopezgConvo1.Interfaces
{
    
        public interface IFromFile<T>
        {
            T FromXml(string filepath);
           
        }
    
}
