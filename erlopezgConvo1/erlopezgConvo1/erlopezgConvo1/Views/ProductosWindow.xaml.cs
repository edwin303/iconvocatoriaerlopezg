﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using erlopezgConvo1.Controllers;
using erlopezgConvo1.Models;

namespace erlopezgConvo1.Views
{
    /// <summary>
    /// Interaction logic for ProductosWindow.xaml
    /// </summary>
    public partial class ProductosWindow : Window
    {
        ProductController pc;
        public ProductosWindow()
        {
            InitializeComponent();
        }

        public void SetData(Pedido data)
        {
            PedidoTextBox.DataContext = data;
            ClienteTextBox.DataContext = data;
            
        }

        
        protected void SetupController()
        {
            pc = new ProductController(this);
            this.SaveButton.Click += new RoutedEventHandler(pc.PersonEventHandler);
            this.SelectButton.Click += new RoutedEventHandler(pc.PersonEventHandler);
            this.OpenButton.Click += new RoutedEventHandler(pc.PersonEventHandler);
        }

     

        
        private void ComboBoxItem_Selected(object sender, RoutedEventArgs e)
        {

        }

        
    }
}
