﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using erlopezgConvo1.Models;
using erlopezgConvo1.Interfaces;
using erlopezgConvo1.Tools.Serialization;


namespace erlopezgConvo1.Models
{
    [Serializable]
    public class Pedido : IToFile, IFromFile<Pedido>
    {
        public int NumPedido { get; set; }

        String Productos = new List<string>
        {
            new Pedido() {Nombre = "" }
        };
  
        public Usuario Nombre { get; set; }
        public Pedido FromXml(string filepath)
        {
            return XmlSerialization.ReadFromXmlFile<Pedido>(filepath);
        }

        public void ToXml(string filepath)
        {
            XmlSerialization.WriteToXmlFile(filepath, this);
        }

      
    }
}
